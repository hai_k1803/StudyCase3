package com.haikd;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;

import com.haikd.domain.Account;
import com.haikd.repositories.AccountRepository;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Springboot2WebappJspApplication {

	@Autowired
	private AccountRepository accountRepository;
	@Value("${spring.csv.path}")
	private String csvFile;
	@Autowired
	private ResourceLoader resourceLoader;

	@Bean
	public Java8TimeDialect java8TimeDialect() {
		return new Java8TimeDialect();
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Springboot2WebappJspApplication.class);
		app.setDefaultProperties(Collections.singletonMap("server.servlet.context-path", "/myTest"));
		app.run(args);
	}

	@Bean
	@Profile("!test & !integration")
	public CommandLineRunner run(ApplicationContext appContext) {
		return args -> {
			try {
				List<Account> accounts = this.readAccountsFromFile(csvFile);
				if (accounts.isEmpty()) {
					return;
				}
				accountRepository.saveAll(accounts);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		};
	}

	private List<Account> readAccountsFromFile(String csvFile) throws IOException {
		List<Account> accounts = new ArrayList<Account>();

		File file = this.getFile(csvFile);
		String cvsSplitBy = ",";
		try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()))) {
			stream.collect(Collectors.toList()).forEach(line -> {
				Account account = new Account();
				String[] lineData = line.split(cvsSplitBy);
				account.setAccountNumber(lineData[1]);
				account.setName(lineData[0]);
				account.setBalance(new BigDecimal(lineData[2]));
				account.setPin(lineData[3]);
				accounts.add(account);
			});
		}

		return accounts;
	}

	private File getFile(String csvFile) throws IOException {
		Resource resource = resourceLoader.getResource("classpath:" + csvFile);
		return resource.getFile();
	}
}