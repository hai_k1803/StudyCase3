package com.haikd.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.haikd.domain.Account;
import com.haikd.repositories.AccountRepository;
import com.haikd.repositories.TransactionRepository;

public abstract class CommonTransactionService {

	@Autowired
	protected TransactionRepository transactionDao;
	@Autowired
	protected AccountRepository accountDao;

	public Account getAccountBy(String accNum) {
		List<Account> accounts = this.accountDao.findByAccountNumber(accNum);
		if (accounts.isEmpty()) {
			return null;
		}
		return accounts.get(0);
	}

	protected void subtract(BigDecimal amount, Account currentAccount) throws Exception {
		BigDecimal userBalance = currentAccount.getBalance();
		currentAccount.setBalance(userBalance.subtract(amount));
		this.accountDao.save(currentAccount);
	}

	protected boolean checkIfInsufficientBalance(String amount, String accNum) {
		Account currentAcc = getAccountBy(accNum);
		if (currentAcc.isBalanceInsuffient(amount)) {
			return true;
		}
		return false;
	}
}
