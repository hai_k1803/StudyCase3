package com.haikd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.haikd.domain.Transaction;
import com.haikd.repositories.TransactionRepository;

@Service
public class TransactionService {

	@Autowired
	private TransactionRepository transactionDao;

	public List<Transaction> getLastTransactions(String accountNumber) {
		return this.transactionDao
				.findTop10ByDestinationAccountNumOrCurrentAccountAccountNumberOrderByDateDesc(accountNumber, accountNumber);
	}

	public Page<Transaction> findPaginated(Pageable pageable, String accNum) {
		return this.transactionDao.findAllByDestinationAccountNumOrCurrentAccountAccountNumber(accNum, accNum,
				pageable);
	}
}