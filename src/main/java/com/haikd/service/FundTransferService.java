package com.haikd.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haikd.domain.Account;
import com.haikd.domain.Transaction;
import com.haikd.utils.Constant;
import com.haikd.utils.TransactionFactory;
import com.haikd.utils.Util;

@Service
public class FundTransferService extends CommonTransactionService {

	public String generateReference() {
		String referenceNum;
		boolean isExist = false;
		do {
			referenceNum = Util.generateReference();
			isExist = this.checkExistenceOf(referenceNum);
		} while (isExist);

		return referenceNum;
	}

	private boolean checkExistenceOf(String referenceNum) {
		List<Transaction> transactions = this.transactionDao.findByReferenceNumber(referenceNum);
		if (transactions.isEmpty()) {
			return false;
		}

		return true;
	}

	@Transactional(rollbackFor = Exception.class)
	public void transfer(Transaction transaction) throws Exception {
		BigDecimal amount = transaction.getAmount();
		this.subtractTransferingAccount(amount, transaction.getCurrentAccount());
		this.addReceivingAccount(amount, transaction.getDestinationAccountNum());
		this.transactionDao.save(transaction);
	}

	private void subtractTransferingAccount(BigDecimal amount, Account currentAccount) {
		BigDecimal userBalance = currentAccount.getBalance();
		currentAccount.setBalance(userBalance.subtract(amount));
		this.accountDao.save(currentAccount);
	}

	private void addReceivingAccount(BigDecimal amount, String destinationAccountNum) {
		Account destinationAcc = this.getAccountBy(destinationAccountNum);
		destinationAcc.setBalance(destinationAcc.getBalance().add(amount));
		this.accountDao.save(destinationAcc);
	}

	public Transaction createTransaction(String accountNumber, String destAccountNum) {
		Account currentAccount = this.getAccountBy(accountNumber);
		Transaction transaction = TransactionFactory.create(Constant.TYPES_OF_TRANSACTIONS.get(Constant.TRANSFER),
				LocalDateTime.now(), currentAccount);
		transaction.setDestinationAccountNum(destAccountNum);
		return transaction;
	}

}