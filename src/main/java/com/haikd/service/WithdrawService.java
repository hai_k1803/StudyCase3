package com.haikd.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haikd.domain.Account;
import com.haikd.domain.Transaction;
import com.haikd.utils.Constant;
import com.haikd.utils.TransactionFactory;

@Service
public class WithdrawService extends CommonTransactionService {

	@Transactional(rollbackFor = Exception.class)
	public Transaction withdraw(String rawAmount, String accNum) throws Exception {
		Account currentAccount = this.getAccountBy(accNum);
		Transaction transaction = TransactionFactory.create(Constant.TYPES_OF_TRANSACTIONS.get(Constant.WITHDRAW),
				LocalDateTime.now(), currentAccount);
		BigDecimal amount = new BigDecimal(rawAmount);
		transaction.setAmount(amount);
		this.subtract(amount, currentAccount);
		this.transactionDao.save(transaction);
		return transaction;
	}
}