package com.haikd.controllers;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.haikd.domain.Transaction;
import com.haikd.service.FundTransferService;
import com.haikd.utils.Constant;
import com.haikd.utils.FundTransferValidator;

@Controller
@RequestMapping("/transfer")
public class FundTransferController {

	private static final String TRANSFER_FOLDER_PATH = "fundTransfer/";
	@Autowired
	private FundTransferService fundTransferService;
	@Autowired
	private FundTransferValidator validator;


	@RequestMapping("")
	public String openTransferPage() {
		return TRANSFER_FOLDER_PATH + "destAccountInput";
	}

	@RequestMapping("/validateAccount")
	public String checkValidAccount(String destAccountNum, HttpServletRequest request, ModelMap model) {
		this.validator.validateAccount(destAccountNum);
		if (this.validator.hasError()) {
			model.addAttribute(Constant.ERROR_HOLDER, this.validator.getError());
			return TRANSFER_FOLDER_PATH + "destAccountInput";
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Transaction transaction = this.fundTransferService.createTransaction(auth.getName(), destAccountNum);
		request.getSession().setAttribute(Constant.TRANSACTION, transaction);
		return TRANSFER_FOLDER_PATH + "amountInput";
	}

	@RequestMapping("/generateReference")
	public String generateReference(HttpServletRequest request, ModelMap model) {
		String referenceNumber = this.fundTransferService.generateReference();
		Transaction transaction = (Transaction) request.getSession().getAttribute(Constant.TRANSACTION);
		transaction.setReferenceNumber(referenceNumber);
		model.addAttribute("referenceNumber", referenceNumber);
		return TRANSFER_FOLDER_PATH + "referenceGeneration";
	}

	@RequestMapping("/showConfirm")
	public String showConfirm(HttpServletRequest request, ModelMap model) {
		Transaction transaction = (Transaction) request.getSession().getAttribute(Constant.TRANSACTION);
		model.addAttribute(Constant.TRANSACTION, transaction);
		return TRANSFER_FOLDER_PATH + "confirm";
	}

	@RequestMapping("/confirm")
	public String confirmTransaction(HttpServletRequest request, ModelMap model) {
		Transaction transaction = (Transaction) request.getSession().getAttribute(Constant.TRANSACTION);
		try {
			this.fundTransferService.transfer(transaction);
			model.addAttribute(Constant.TRANSACTION, transaction);
			model.addAttribute("balance", transaction.getCurrentAccount().getBalance());
			return TRANSFER_FOLDER_PATH + "summary";
		} catch (Exception e) {
			model.addAttribute("error", e.getMessage());
			return "fundTransfer/error";
		}
	}

	@RequestMapping("/cancel")
	public String cancelTransaction(HttpServletRequest request) {
		request.getSession().setAttribute(Constant.TRANSACTION, null);
		return "transaction";
	}

	@RequestMapping("/checkValidAmount")
	public String checkValidAmount(HttpServletRequest request, ModelMap model, String amount) {
		// validate amount
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		this.validator.validateAmount(amount, auth.getName());
		if (this.validator.hasError()) {
			model.addAttribute(Constant.ERROR_HOLDER, this.validator.getError());
			return TRANSFER_FOLDER_PATH + "amountInput";
		}

		Transaction transaction = (Transaction) request.getSession().getAttribute(Constant.TRANSACTION);
		transaction.setAmount(new BigDecimal(amount));
		return "redirect:/transfer/generateReference";
	}
}
