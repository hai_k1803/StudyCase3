package com.haikd.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.haikd.utils.Constant;
import com.haikd.utils.OtherWithdrawValidator;

@Controller
@RequestMapping("/other")
public class OtherWithdrawController extends CommonWithdrawController {

	@Autowired
	private OtherWithdrawValidator otherWithdrawValidator;

	@RequestMapping("")
	public String openOtherWithdraw() {
		return "otherWithdraw";
	}

	@RequestMapping("/doWithdraw")
	public String withdraw(ModelMap model, String amount, RedirectAttributes redirectAttrs) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String accountNumber = auth.getName();
		this.otherWithdrawValidator.validate(amount, accountNumber);
		if (this.otherWithdrawValidator.hasError()) {
			model.addAttribute(Constant.ERROR_HOLDER, this.otherWithdrawValidator.getError());
			model.addAttribute("amount", amount);
			return "otherWithdraw";
		}

		try {
			this.withdraw(accountNumber, amount, redirectAttrs);
			return "redirect:/withdraw/summaryWithdraw";
		} catch (Exception e) {
			model.addAttribute("error", e.getMessage());
			return "error";
		}
	}

}
