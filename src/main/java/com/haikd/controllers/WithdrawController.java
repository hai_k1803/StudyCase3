package com.haikd.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.haikd.utils.Constant;
import com.haikd.utils.WithdrawValidator;

@Controller
@RequestMapping("/withdraw")
public class WithdrawController extends CommonWithdrawController {

	@Autowired
	private WithdrawValidator withdrawValidator;

	@RequestMapping("")
	public String openWelcomePage() {
		return "withdraw";
	}

	@RequestMapping("/doWithdraw")
	public String withdraw(ModelMap model, String amount, RedirectAttributes redirectAttrs) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String accountNumber = auth.getName();
		this.withdrawValidator.validate(amount, accountNumber);
		if (this.withdrawValidator.hasError()) {
			model.addAttribute(Constant.ERROR_HOLDER, this.withdrawValidator.getError());
			return "withdraw";
		}

		try {
			this.withdraw(accountNumber, amount, redirectAttrs);
			return "redirect:/withdraw/summaryWithdraw";
		} catch (Exception e) {
			model.addAttribute("error", e.getMessage());
			return "error";
		}
	}

	@RequestMapping("/summaryWithdraw")
	public String summaryWithdraw(ModelMap model) {
		return "withdrawSummary";
	}

	@RequestMapping("/back")
	public String goback() {
		return "transaction";
	}
}