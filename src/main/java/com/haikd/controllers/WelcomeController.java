package com.haikd.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.haikd.domain.Account;
import com.haikd.utils.Constant;

@Controller
public class WelcomeController {

	@RequestMapping(value = { "/login", "/" })
	public String login(@RequestParam(value = "error", required = false) String error, ModelMap model) {
		model.addAttribute("loginForm", new Account());
		if (error != null) {
			model.addAttribute("error", "Invalid Account Number/PIN");
		}

		return Constant.WELCOME_PAGE;
	}

}
