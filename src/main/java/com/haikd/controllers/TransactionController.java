package com.haikd.controllers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.haikd.domain.Transaction;
import com.haikd.service.TransactionService;
import com.haikd.utils.Constant;

@Controller
@RequestMapping("/transaction")
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	@RequestMapping("")
	public String openTransactionPage() {
		return Constant.TRANSACTION_PAGE;
	}

	@RequestMapping("/getLastTransactions")
	public String getLastTransactions(ModelMap model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		List<Transaction> transactions = this.transactionService.getLastTransactions(auth.getName());
		model.addAttribute("transactions", transactions);
		return Constant.LAST_TRANSACTIONS_PAGE;
	}

	@RequestMapping("/getAllTransactions/page/{page}")
	public String getAllTransactions(@PathVariable("page") Optional<Integer> page, ModelMap model) {
		int currentPage = page.orElse(1);
		int pageSize = 10;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Page<Transaction> transactions = this.transactionService
				.findPaginated(PageRequest.of(currentPage - 1, pageSize), auth.getName());
		model.addAttribute("transactions", transactions.getContent());

		int totalPages = transactions.getTotalPages();
		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
			model.addAttribute("pageNumbers", pageNumbers);
		}
		return Constant.ALL_TRANSACTIONS_PAGE;
	}
}
