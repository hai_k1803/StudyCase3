package com.haikd.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.haikd.domain.Account;
import com.haikd.domain.Transaction;
import com.haikd.service.WithdrawService;

@Controller
public abstract class CommonWithdrawController {

	@Autowired
	protected WithdrawService withdrawService;

	protected void withdraw(String accNum, String amount, RedirectAttributes redirectAttrs) throws Exception {
		Transaction transaction = this.withdrawService.withdraw(amount, accNum);
		redirectAttrs.addFlashAttribute("transaction", transaction);
		Account currentAcc = this.withdrawService.getAccountBy(accNum);
		redirectAttrs.addFlashAttribute("balance", currentAcc.getBalance());
	}

	protected void setSummaryData(ModelMap model, Transaction transaction) {
		model.addAttribute("transaction", transaction);
		Account currentAcc = this.withdrawService.getAccountBy(transaction.getCurrentAccount().getAccountNumber());
		model.addAttribute("balance", currentAcc.getBalance());
	}
}
