package com.haikd.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.haikd.domain.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

	public List<Transaction> findByReferenceNumber(String referenceNum);

	public List<Transaction> findTop10ByDestinationAccountNumOrCurrentAccountAccountNumberOrderByDateDesc(
			String destAccNum, String accountNumber);

	public Page<Transaction> findAllByDestinationAccountNumOrCurrentAccountAccountNumber(String destAccNum,
			String accountNumber, Pageable pageable);
}
