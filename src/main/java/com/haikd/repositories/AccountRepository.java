package com.haikd.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.haikd.domain.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

	List<Account> findByAccountNumberAndPin(String accountNumber, String pin);

	List<Account> findByAccountNumber(String accountNumber);	
}