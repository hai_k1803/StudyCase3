package com.haikd.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.Proxy;
import org.hibernate.validator.constraints.Length;

@Entity
@Proxy(lazy = false)
@Table(name = "ACCOUNT")
public class Account implements Serializable {
	private static final long serialVersionUID = 1876737674562485482L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name", length = 128, nullable = false)
	private String name;

	@Column(name = "pin", length = 6, nullable = false)
	@NotBlank(message = "Pin number is mandatory")
	@Length(min = 6, max = 6, message = "PIN should have 6 digits length")
	@Pattern(regexp = "^[0-9]*$", message = "PIN should only contains numbers")
	private String pin;

	@Column(name = "balance", precision = 10, scale = 2, nullable = false)
	private BigDecimal balance;

	@Column(name = "account_number", length = 6, nullable = false, unique = true)
	@NotBlank(message = "Account number is mandatory")
	@Length(min = 6, max = 6, message = "Account number should have 6 digits length")
	@Pattern(regexp = "^[0-9]*$", message = "Account number should only contains numbers")
	private String accountNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public BigDecimal account() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public boolean isBalanceInsuffient(String amount) {
		BigDecimal amountInNumber = new BigDecimal(amount);
		if (amountInNumber.compareTo(balance) > 0) {
			return true;
		}
		return false;
	}
}