package com.haikd.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Proxy(lazy = false)
@Table(name = "TRANSACTIONS")
public class Transaction implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7548778320127873136L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "type", nullable = false)
	private int type;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "current_account")
	private Account currentAccount;

	@Column(name = "destination_account_num", length = 6)
	private String destinationAccountNum;

	@Column(name = "amount", precision = 10, scale = 2, nullable = false)
	private BigDecimal amount;

	@Column(name = "reference_number", length = 6)
	private String referenceNumber;

	@Column(name = "date", nullable = false)
	private LocalDateTime date;

	public Transaction(int type, LocalDateTime date, Account loginedAccount) {
		this.type = type;
		this.date = date;
		this.currentAccount = loginedAccount;
	}

	// Just for JPA
	public Transaction() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Account getCurrentAccount() {
		return currentAccount;
	}

	public void setCurrentAccount(Account currentAccount) {
		this.currentAccount = currentAccount;
	}

	public String getDestinationAccountNum() {
		return destinationAccountNum;
	}

	public void setDestinationAccountNum(String destinationAccountNum) {
		this.destinationAccountNum = destinationAccountNum;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
}