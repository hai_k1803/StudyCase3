package com.haikd.utils;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import com.haikd.domain.Account;

@Component
public class FundTransferValidator extends WithdrawValidator {

	public void validateAccount(String destinationAccNum) {
		this.error = null;
		if (!this.checkIfNumber(destinationAccNum)) {
			this.error = "Invalid account";
			return;
		}
		Account destinationAccount = this.getAccountBy(destinationAccNum);
		if (destinationAccount == null) {
			this.error = "Invalid account";
		}
	}

	public void validateAmount(String amount, String accNum) {
		this.error = null;
		super.validateAmount(amount);
		if (this.hasError())
			return;
		BigDecimal amountInNumber = new BigDecimal(amount);
		if (Util.checkIfOverMaxAmount(amountInNumber)) {
			this.error = "Maximum amount to withdraw is $" + Constant.MAX_AMOUNT;
			return;
		}
		if (checkIfUnderMinAmount(amountInNumber)) {
			this.error = "Minimum amount to withdraw is $" + Constant.MIN_AMOUNT;
			return;
		}
		checkInsufficientBalance(amount, accNum);
	}

	private boolean checkIfUnderMinAmount(BigDecimal amount) {
		if (Constant.MIN_AMOUNT.compareTo(amount) > 0) {
			return true;
		}
		return false;
	}

}