package com.haikd.utils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.haikd.domain.Account;
import com.haikd.repositories.AccountRepository;

@Component
public class WithdrawValidator {

	private static final String NUMBER_PATTERN = "\\d+";
	@Autowired
	private AccountRepository accountRepo;
	protected String error;

	public String getError() {
		return error;
	}

	public boolean hasError() {
		if (this.error == null) {
			return false;
		}
		return true;
	}

	public void validate(String amount, String accNum) {
		this.error = null;
		this.validateAmount(amount);
		if (this.hasError())
			return;
		this.checkInsufficientBalance(amount, accNum);
	}

	protected void checkInsufficientBalance(String amount, String accNum) {
		Account currentAcc = getAccountBy(accNum);
		if (currentAcc.isBalanceInsuffient(amount)) {
			this.error = "Insufficient balance $" + currentAcc.getBalance();
		}
	}

	protected Account getAccountBy(String accNum) {
		List<Account> accounts = this.accountRepo.findByAccountNumber(accNum);
		if (accounts.isEmpty()) {
			return null;
		}
		return accounts.get(0);
	}

	public boolean checkIfNumber(String input) {
		if (input.matches(NUMBER_PATTERN))
			return true;

		return false;
	}

	protected void validateAmount(String amount) {
		if (!checkIfNumber(amount)) {
			error = "Invalid amount";
		}
	}

}