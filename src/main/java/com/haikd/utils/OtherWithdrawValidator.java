package com.haikd.utils;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class OtherWithdrawValidator extends WithdrawValidator {

	@Override
	public void validateAmount(String amount) {
		super.validateAmount(amount);
		if (this.hasError())
			return;
		if (notMultipleOf10(amount)) {
			this.error = "Invalid amount";
			return;
		}
		if (Util.checkIfOverMaxAmount(new BigDecimal(amount))) {
			this.error = "Maximum amount to withdraw is $" + Constant.MAX_AMOUNT;
		}
	}

	public boolean notMultipleOf10(String amount) {
		double amountInNumber = Double.valueOf(amount);
		if (amountInNumber % 10 != 0) {
			return true;
		}
		return false;
	}

}