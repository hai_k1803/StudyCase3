package com.haikd.utils;

import java.math.BigDecimal;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.haikd.domain.Transaction;

@Aspect
@Component
public class LoggingAspect {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@After(value = "execution(* com.haikd.service.FundTransferService.transfer(..))")
	public void afterTransfer(JoinPoint joinPoint) {
		log.info(joinPoint.getSignature().toString());
		Object[] obj = joinPoint.getArgs();
		Transaction trans = (Transaction) obj[0];
		String currentAcc = trans.getCurrentAccount().getAccountNumber();
		String receivingAcc = trans.getDestinationAccountNum();
		BigDecimal amount = trans.getAmount();
		log.info("Account " + currentAcc + " transfered " + " $" + amount + " to account " + receivingAcc);
	}

	@After(value = "execution(* com.haikd.service.WithdrawService.withdraw(..))")
	public void afterWithdraw(JoinPoint joinPoint) {
		log.info(joinPoint.getSignature().toString());
		Object[] obj = (Object[]) joinPoint.getArgs();
		log.info("Account " + obj[1] + " withdraw " + " $" + obj[0]);
	}

	@AfterThrowing(value = "execution(* com.haikd.service.FundTransferService.transfer(..))"
			+ " || execution(* com.haikd.service.WithdrawService.withdraw(..))", throwing = "e")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature().getDeclaringTypeName(),
				joinPoint.getSignature().getName(), e.getCause() != null ? e.getCause() : "NULL");
	}

}
