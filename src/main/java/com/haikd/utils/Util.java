package com.haikd.utils;

import java.math.BigDecimal;
import java.util.Random;

public class Util {
	public static String generateReference() {
		StringBuilder reference = new StringBuilder();
		String range = "1234567890";
		Random rnd = new Random();
		while (reference.length() < 6) { // length of the random string.
			int index = (int) (rnd.nextFloat() * range.length());
			reference.append(range.charAt(index));
		}
		return reference.toString();
	}

	public static boolean checkIfOverMaxAmount(BigDecimal amount) {
		if (Constant.MAX_AMOUNT.compareTo(amount) < 0) {
			return true;
		}
		return false;
	}
}