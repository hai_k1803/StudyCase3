package com.haikd.utils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Constant {
	public static final BigDecimal TEN_USD = BigDecimal.valueOf(10);
	public static final BigDecimal FIFTY_USD = BigDecimal.valueOf(50);
	public static final BigDecimal HUNDRED_USD = BigDecimal.valueOf(100);
	public static final String TEN_USD_VALUE = "10";
	public static final String FIFTY_USD_VALUE = "50";
	public static final String HUNDRED_USD_VALUE = "100";
	public static final BigDecimal MAX_AMOUNT = new BigDecimal(1000);
	public static final BigDecimal MIN_AMOUNT = BigDecimal.valueOf(1);
	public static final String WITHDRAW = "WidthDraw";
	public static final String TRANSFER = "Transfer";
	public static final String EMPTY = "";
	public static final String ACCOUNT = "account";
	public static final String TRANSACTION = "transaction";
	public static final String TRANSACTION_PAGE = "transaction";
	public static final String WELCOME_PAGE = "welcome";
	public static final String LAST_TRANSACTIONS_PAGE = "lastTransactions";
	public static final String WITHDRAW_PAGE = "withdraw";
	public static final String WITHDRAW_SUMMARY_PAGE = "withdrawSummary";
	public static final String ERROR_HOLDER = "error";
	public static final Map<String, Integer> TYPES_OF_TRANSACTIONS = new HashMap<String, Integer>() {
		{
			put(WITHDRAW, 1);
			put(TRANSFER, 2);
		}
	};
	public static final String ALL_TRANSACTIONS_PAGE = "allTransactions";

}