package com.haikd.utils;

import java.time.LocalDateTime;
import com.haikd.domain.Account;
import com.haikd.domain.Transaction;

public class TransactionFactory {
	public static Transaction create(int type, LocalDateTime date, Account loginedAccount) {
		return new Transaction(type, date, loginedAccount);
	}
}
