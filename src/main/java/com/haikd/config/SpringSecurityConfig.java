package com.haikd.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;

	@SuppressWarnings("deprecation")
	@Bean
	public static NoOpPasswordEncoder passwordEncoder() {
		return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
	}

	// Secure the endpoins with HTTP Basic authentication
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.authorizeRequests().antMatchers("/transaction/**").authenticated().antMatchers("/withdraw/**")
				.authenticated().antMatchers("/transfer/**").authenticated().antMatchers("/other/**").authenticated();
		http.authorizeRequests().and().formLogin()//
				.loginPage("/login")//
				.usernameParameter("accountNumber").passwordParameter("pin").defaultSuccessUrl("/transaction")//
				.failureUrl("/login?error=true");
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery(
						"select account_number as accountNumber, pin, 1  from account where account_number=?")
				.authoritiesByUsernameQuery(
						"select account_number as accountNumber,'ROLE_USER' from account where account_number=?");
	}

}
