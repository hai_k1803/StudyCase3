package com.haikd.service;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.haikd.domain.Account;
import com.haikd.domain.Transaction;
import com.haikd.repositories.AccountRepository;
import com.haikd.repositories.TransactionRepository;

@RunWith(SpringRunner.class)
public class FundTransferServiceTest {
	final static String SIX_ALL_NUMBERS_PATTERN = "\\d{6}";
	@Mock
	AccountRepository accountRepo;
	@Mock
	TransactionRepository transRepo;
	@InjectMocks
	FundTransferService service;

	/*
	 * When creating a transfer transaction from account1 to account2, it's expected
	 * to have account1's balance reduced by the amount of the transaction and have
	 * account2's balance increased by the amount of the transaction
	 */
	@Test
	public void testTransferMoneyFromAnAccountToAnother() throws Exception {
		String amount = "20";
		BigDecimal transferingAccountBalance = new BigDecimal(1000);
		BigDecimal receivingAccountBalance = new BigDecimal(20);
		Account transferingAccount = new Account();
		transferingAccount.setBalance(transferingAccountBalance);
		List<Account> receivingAccs = new ArrayList<Account>();
		Account receivingAccount = new Account();
		receivingAccount.setBalance(receivingAccountBalance);
		receivingAccs.add(receivingAccount);
		Transaction mockTrans = new Transaction();
		mockTrans.setCurrentAccount(transferingAccount);
		mockTrans.setAmount(new BigDecimal(amount));
		mockTrans.setReferenceNumber(receivingAccount.getAccountNumber());
		when(this.accountRepo.save(transferingAccount)).thenReturn(transferingAccount);
		when(this.accountRepo.findByAccountNumber(receivingAccount.getAccountNumber())).thenReturn(receivingAccs);
		when(this.accountRepo.save(receivingAccount)).thenReturn(receivingAccount);
		this.service.transfer(mockTrans);
		assertEquals(transferingAccountBalance.subtract(new BigDecimal(amount)), transferingAccount.getBalance());
		assertEquals(receivingAccountBalance.add(new BigDecimal(amount)), receivingAccount.getBalance());
	}

	@Test
	public void givenATransaction_whenGenerateReference_thenReturnStringContains6Numbers() {
		List<Transaction> emptyTransactions = new ArrayList<Transaction>();
		when(this.transRepo.findByReferenceNumber(anyString())).thenReturn(emptyTransactions);
		String reference = this.service.generateReference();
		assertTrue(reference.matches(SIX_ALL_NUMBERS_PATTERN));
	}

	@Test
	public void testCreateTransaction() {
		String accountNumber = "123456";
		Account acc = new Account();
		acc.setAccountNumber(accountNumber);
		List<Account> accs = new ArrayList<Account>();
		accs.add(acc);
		String destAccountNum = "111111";
		when(this.accountRepo.findByAccountNumber(accountNumber)).thenReturn(accs);
		Transaction trans = this.service.createTransaction(accountNumber, destAccountNum);
		assertEquals(accountNumber, trans.getCurrentAccount().getAccountNumber());
		assertEquals(destAccountNum, trans.getDestinationAccountNum());
	}

}
