package com.haikd.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import com.haikd.domain.Transaction;
import com.haikd.repositories.TransactionRepository;

@RunWith(SpringRunner.class)
public class TransactionServiceTest {

	@Mock
	TransactionRepository repo;
	@InjectMocks
	TransactionService service;

	@Test
	public void testGetLastTransactions() {
		String accNum = "any";
		List<Transaction> mockTransactions = this.initTransactions();
		Mockito.when(repo.findTop10ByDestinationAccountNumOrCurrentAccountAccountNumberOrderByDateDesc(accNum, accNum))
				.thenReturn(mockTransactions);
		List<Transaction> transactions = this.service.getLastTransactions(accNum);
		assertTrue(transactions.size() == 3);
	}

	private List<Transaction> initTransactions() {
		List<Transaction> transactions = new ArrayList<Transaction>();
		Transaction trans1 = new Transaction();
		Transaction trans2 = new Transaction();
		Transaction trans3 = new Transaction();
		transactions.add(trans1);
		transactions.add(trans2);
		transactions.add(trans3);

		return transactions;
	}

}
