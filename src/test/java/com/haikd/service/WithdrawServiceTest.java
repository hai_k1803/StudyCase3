package com.haikd.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.haikd.domain.Account;
import com.haikd.domain.Transaction;
import com.haikd.repositories.AccountRepository;
import com.haikd.repositories.TransactionRepository;

@RunWith(SpringRunner.class)
public class WithdrawServiceTest {

	@Mock
	AccountRepository accountRepo;
	@Mock
	TransactionRepository transRepo;
	@InjectMocks
	WithdrawService service;

	/**
	 * When creating a withdraw transaction to an account, this account is expected
	 * to have its balance reduced by the amount indicated in the withdraw
	 * 
	 * @throws Exception
	 */
	@Test
	public void testAnAccountBalaceIsReducedWhenWithdraw() throws Exception {
		String amount = "10";
		BigDecimal balance = new BigDecimal(1000);
		List<Account> accounts = new ArrayList<Account>();
		Account currentAccount = new Account();
		currentAccount.setBalance(balance);
		accounts.add(currentAccount);
		when(this.accountRepo.findByAccountNumber(currentAccount.getAccountNumber())).thenReturn(accounts);
		when(this.accountRepo.save(any(Account.class))).thenReturn(currentAccount);
		when(this.transRepo.save(any(Transaction.class))).thenReturn(any(Transaction.class));
		this.service.withdraw(amount, currentAccount.getAccountNumber());
		verify(accountRepo, times(1)).save(currentAccount);
		verify(transRepo, times(1)).save(any(Transaction.class));
		assertEquals(balance.subtract(new BigDecimal(amount)), currentAccount.getBalance());
	}

}
