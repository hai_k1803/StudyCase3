package com.haikd.integration.test;

import static com.haikd.constant.Constant.MOCK_USER;
import static com.haikd.constant.Constant.TRANSACTION;
import static com.haikd.constant.Constant.TRANSFER_PATH;
import static com.haikd.constant.Constant.WITHDRAW_PATH;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;

import com.haikd.domain.Account;
import com.haikd.domain.Transaction;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration")
public class TransactionFetchingIT extends CommonIT {

	private static final int SECOND_LASTEST_RECORD = 1;
	private static final int LASTEST_RECORD = 0;
	Account account;

	@Before
	public void setUp() {
		super.setUp();
		createAndSaveToDb();
		createTransactionAndSaveToDb();
	}

	private void createTransactionAndSaveToDb() {
		Transaction trans = new Transaction();
		trans.setCurrentAccount(account);
		trans.setDate(LocalDateTime.now());
		trans.setType(1);
		trans.setAmount(BigDecimal.valueOf(100));
		tranRepsitory.save(trans);
	}

	private void createAndSaveToDb() {
		account = new Account();
		account.setAccountNumber(MOCK_USER);
		account.setPin(MOCK_USER);
		account.setName("mock");
		account.setBalance(BigDecimal.valueOf(60000));
		accRepository.save(account);
	}

	/*
	 * When creating a successful transaction, it's expected to have this
	 * transaction when getting list of last 10 transactions
	 */
	@WithMockUser(MOCK_USER)
	@Test
	public void givenTransactions_whenFetchLastTransactions_thenTransactionIncludedInList() throws Exception {
		String amount = "200";
		List<Transaction> top10TransactionsBefore = (List<Transaction>) tranRepsitory.findAll();
		Account account2 = createAndSaveAcc2ToDb();
		Map<String, Object> sessionattr = mockTransaction(account2);

		mockMvc.perform(get(TRANSFER_PATH + "/validateAccount").sessionAttrs(sessionattr).param("destAccountNum",
				account2.getAccountNumber()));
		mockMvc.perform(get(TRANSFER_PATH + "/checkValidAmount").sessionAttrs(sessionattr).param("amount", amount));
		mockMvc.perform(get(TRANSFER_PATH + "/generateReference").sessionAttrs(sessionattr));
		mockMvc.perform(get(TRANSFER_PATH + "/confirm").sessionAttrs(sessionattr));

		List<Transaction> top10TransactionsAfter = (List<Transaction>) tranRepsitory
				.findTop10ByDestinationAccountNumOrCurrentAccountAccountNumberOrderByDateDesc(
						account.getAccountNumber(), account.getAccountNumber());
		assertTrue(top10TransactionsAfter.size() == top10TransactionsBefore.size() + 1);
		assertNewlyAddedTransactionIncludedIn(top10TransactionsAfter, amount, account2);
	}

	private Account createAndSaveAcc2ToDb() {
		Account account2 = new Account();
		account2.setAccountNumber("222222");
		account2.setPin("222222");
		account2.setName("Acc2");
		account2.setBalance(new BigDecimal(200));
		accRepository.save(account2);
		return account2;
	}

	private Map<String, Object> mockTransaction(Account account2) {
		Transaction trans = new Transaction();
		trans.setCurrentAccount(account);
		trans.setDestinationAccountNum(account2.getAccountNumber());
		trans.setDate(LocalDateTime.now());
		trans.setType(2);
		Map<String, Object> sessionattr = new HashMap<String, Object>();
		sessionattr.put(TRANSACTION, trans);
		return sessionattr;
	}

	private void assertNewlyAddedTransactionIncludedIn(List<Transaction> top10Transactions, String amount,
			Account account2) {
		Transaction lastestTransaction = top10Transactions.get(LASTEST_RECORD);

		assertTrue(account.getAccountNumber().equals(lastestTransaction.getCurrentAccount().getAccountNumber()));
		assertTrue(account2.getAccountNumber().equals(lastestTransaction.getDestinationAccountNum()));
		assertTrue(lastestTransaction.getType() == 2); // 2 = fund transfer type
		assertTrue((new BigDecimal(amount)).compareTo(lastestTransaction.getAmount()) == 0);
	}

	/*
	 * When creating a bunch of successful transactions, it's expected to have this
	 * list of transactions when getting list of last 10 transactions
	 */
	@WithMockUser(MOCK_USER)
	@Test
	public void givenTransactions_whenFetchLastTransactions_thenTransactionsCreatedIncludedInList() throws Exception {
		String amount1 = "200";
		String amount2 = "40";
		List<Transaction> last10TransactionsBefore = (List<Transaction>) tranRepsitory.findAll();

		mockMvc.perform(get(WITHDRAW_PATH + "/doWithdraw").param("amount", amount1));
		mockMvc.perform(get(WITHDRAW_PATH + "/doWithdraw").param("amount", amount2));

		List<Transaction> last10TransactionsAfter = (List<Transaction>) tranRepsitory
				.findTop10ByDestinationAccountNumOrCurrentAccountAccountNumberOrderByDateDesc(
						account.getAccountNumber(), account.getAccountNumber());
		assertTrue(last10TransactionsAfter.size() == last10TransactionsBefore.size() + 2);
		assertNewlyAddedTransactionsIncludedIn(last10TransactionsAfter, amount1, amount2);
	}

	private void assertNewlyAddedTransactionsIncludedIn(List<Transaction> top10Transactions, String amount1,
			String amount2) {
		Transaction lastestTransaction = top10Transactions.get(LASTEST_RECORD);
		assertTrue(account.getAccountNumber().equals(lastestTransaction.getCurrentAccount().getAccountNumber()));
		assertTrue(lastestTransaction.getType() == 1); // 1 = withdraw type
		assertTrue((new BigDecimal(amount2)).compareTo(lastestTransaction.getAmount()) == 0);

		Transaction secondLastTransaction = top10Transactions.get(SECOND_LASTEST_RECORD);
		assertTrue(account.getAccountNumber().equals(secondLastTransaction.getCurrentAccount().getAccountNumber()));
		assertTrue(secondLastTransaction.getType() == 1); // 1 = withdraw type
		assertTrue((new BigDecimal(amount1)).compareTo(secondLastTransaction.getAmount()) == 0);
	}

	/*
	 * When creating more than 10 transactions, it's expected to have the 10 most
	 * recent transactions when getting list of last 10 transactions
	 */
	@WithMockUser(MOCK_USER)
	@Test
	public void givenMoreThan10Transactions_whenFetchLast10_thenGetRecentTransactions() throws Exception {
		String amount1 = "20";
		String amount2 = "10";

		for (int i = 0; i < 6; i++) {
			mockMvc.perform(get(WITHDRAW_PATH + "/doWithdraw").param("amount", amount1));
			mockMvc.perform(get(WITHDRAW_PATH + "/doWithdraw").param("amount", amount2));
		}

		MvcResult result = mockMvc.perform(get("/transaction/getLastTransactions")).andReturn();
		List<Transaction> lastTransactions = (List<Transaction>) result.getModelAndView().getModel()
				.get("transactions");
		assertTrue(lastTransactions.size() == 10);
		assertNewlyAddedTransactionsIncludedIn(lastTransactions, amount2);
	}

	private void assertNewlyAddedTransactionsIncludedIn(List<Transaction> lastTransactions, String amount) {
		Transaction lastestTransaction = lastTransactions.get(LASTEST_RECORD);
		assertTrue(account.getAccountNumber().equals(lastestTransaction.getCurrentAccount().getAccountNumber()));
		assertTrue(lastestTransaction.getType() == 1); // 1 = withdraw type
		assertTrue((new BigDecimal(amount)).compareTo(lastestTransaction.getAmount()) == 0);
	}
}
