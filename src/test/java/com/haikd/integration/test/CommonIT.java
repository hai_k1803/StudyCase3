package com.haikd.integration.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import com.haikd.domain.Account;
import com.haikd.repositories.AccountRepository;
import com.haikd.repositories.TransactionRepository;

public class CommonIT {
	@Autowired
	MockMvc mockMvc;
	@Autowired
	AccountRepository accRepository;
	@Autowired
	TransactionRepository tranRepsitory;

	@Before
	public void setUp() {
		tranRepsitory.deleteAll();
		accRepository.deleteAll();
	}

	public void assertNotExisted(String accountNumber) {
		List<Account> accounts = accRepository.findByAccountNumber(accountNumber);
		assertTrue(accounts.isEmpty());
	}
}
