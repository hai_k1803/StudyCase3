package com.haikd.integration.test;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.haikd.domain.Account;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration")
public class AccountHandlerIT extends CommonIT {
	/*
	 * When adding an account, this account is expected to be there when getting
	 * user by number account
	 */
	@Test
	public void givenNotExistedAccount_whenAdd_thenOk() {
		Account account = createAccount();
		assertNotExisted(account.getAccountNumber());

		accRepository.save(account);

		assertSuccessfullyAdding(account);
	}

	private void assertSuccessfullyAdding(Account account) {
		List<Account> accounts = accRepository.findByAccountNumber(account.getAccountNumber());
		assertTrue(accounts.size() == 1);
		Account accountFromDb = accounts.get(0);
		assertTrue(account.getAccountNumber().equals(accountFromDb.getAccountNumber()));
		assertTrue(account.getPin().equals(accountFromDb.getPin()));
		assertTrue(account.getName().equals(accountFromDb.getName()));
		assertTrue(account.getBalance().compareTo(accountFromDb.getBalance()) == 0);
	}

	/*
	 * When adding an account, this account is expected to be there when getting
	 * list of accounts
	 */
	@Test
	public void givenAccountList_whenAdd_thenThisAccountInTheList() {
		Account account = createAccount();
		assertNotExisted(account.getAccountNumber());

		accRepository.save(account);

		assertGivenAccountsInclude(account);
	}

	private Account createAccount() {
		Account account = new Account();
		account.setAccountNumber("111115");
		account.setPin("111115");
		account.setName("mock");
		account.setBalance(BigDecimal.TEN);
		return account;
	}

	private void assertGivenAccountsInclude(Account account) {
		List<Account> accounts = (List<Account>) accRepository.findAll();
		assertTrue(checkInclusion(account, accounts));

	}

	private boolean checkInclusion(Account account, List<Account> accounts) {
		return accounts.stream().anyMatch(item -> account.getAccountNumber().equals(item.getAccountNumber()));
	}
}
