package com.haikd.integration.test;

import static com.haikd.constant.Constant.*;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.haikd.domain.Account;
import com.haikd.domain.Transaction;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration")
public class FundTransferIT extends CommonIT {

	Account account1;
	Account account2;

	@Before
	public void setUp() {
		super.setUp();
		account1 = createAndSaveAccount1ToDb();
	}

	/*
	 * When creating a transfer transaction from account1 to account2, it's expected
	 * to have account1's balance reduced by the amount of the transaction and have
	 * account2's balance increased by the amount of the transaction
	 */
	@WithMockUser(MOCK_USER)
	@Test
	public void givenAcc1AndAcc2_whenTransferMoneyFromAcc1ToAcc2_thenAcc1BalanceReducedAndAcc2IncreasedByAmount()
			throws Exception {
		String amount = "100";
		account2 = createAndSaveAcc2ToDb();
		BigDecimal acc1BalanceBeforeTransfer = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();
		BigDecimal acc2BalanceBeforeTransfer = accRepository.findByAccountNumber(account2.getAccountNumber()).get(0)
				.getBalance();
		Map<String, Object> sessionattr = mockTransaction();

		mockMvc.perform(get(TRANSFER_PATH + "/validateAccount").sessionAttrs(sessionattr).param("destAccountNum",
				account2.getAccountNumber()));
		mockMvc.perform(get(TRANSFER_PATH + "/checkValidAmount").sessionAttrs(sessionattr).param("amount", amount));
		mockMvc.perform(get(TRANSFER_PATH + "/generateReference").sessionAttrs(sessionattr));
		mockMvc.perform(get(TRANSFER_PATH + "/confirm").sessionAttrs(sessionattr));

		BigDecimal acc1BalanceAfterTransfer = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();
		BigDecimal acc2BalanceAfterTransfer = accRepository.findByAccountNumber(account2.getAccountNumber()).get(0)
				.getBalance();
		assertTrue(acc1BalanceBeforeTransfer.subtract(new BigDecimal(amount)).compareTo(acc1BalanceAfterTransfer) == 0);
		assertTrue(acc2BalanceBeforeTransfer.add(new BigDecimal(amount)).compareTo(acc2BalanceAfterTransfer) == 0);
	}

	private Map<String, Object> mockTransaction() {
		Transaction trans = new Transaction();
		trans.setCurrentAccount(account1);
		trans.setDestinationAccountNum(account2.getAccountNumber());
		trans.setDate(LocalDateTime.now());
		trans.setType(2);
		Map<String, Object> sessionattr = new HashMap<String, Object>();
		sessionattr.put(TRANSACTION, trans);
		return sessionattr;
	}

	private Account createAndSaveAcc2ToDb() {
		Account account2 = new Account();
		account2.setAccountNumber("222222");
		account2.setPin("222222");
		account2.setName("Acc2");
		account2.setBalance(new BigDecimal(200));
		accRepository.save(account2);
		return account2;
	}

	private Account createAndSaveAccount1ToDb() {
		Account account1 = new Account();
		account1.setAccountNumber(MOCK_USER);
		account1.setPin(MOCK_USER);
		account1.setName("Acc1");
		account1.setBalance(new BigDecimal(600));
		accRepository.save(account1);
		return account1;
	}

	/*
	 * When creating a transfer transaction from account1 to account2, and account1
	 * does not have enough funds, operation should throw an error
	 */
	@WithMockUser(MOCK_USER)
	@Test
	public void givenAcc1NotEnoughFund_whenTransferMoneyFromAcc1ToAcc2_thenFailed() throws Exception {
		String amount = "700";
		account2 = createAndSaveAcc2ToDb();
		BigDecimal acc1BalanceBeforeTransfer = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();
		BigDecimal acc2BalanceBeforeTransfer = accRepository.findByAccountNumber(account2.getAccountNumber()).get(0)
				.getBalance();
		
		Map<String, Object> sessionattr = mockTransaction();
		mockMvc.perform(get(TRANSFER_PATH + "/validateAccount").sessionAttrs(sessionattr).param("destAccountNum",
				account2.getAccountNumber()));
		mockMvc.perform(get(TRANSFER_PATH + "/checkValidAmount").sessionAttrs(sessionattr).param("amount", amount))
				.andExpect(model().attribute(ERROR_HOLDER, "Insufficient balance $" + acc1BalanceBeforeTransfer));
		mockMvc.perform(get(TRANSFER_PATH + "/generateReference").sessionAttrs(sessionattr));
		mockMvc.perform(get(TRANSFER_PATH + "/confirm").sessionAttrs(sessionattr));

		BigDecimal acc1BalanceAfterTransfer = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();
		BigDecimal acc2BalanceAfterTransfer = accRepository.findByAccountNumber(account2.getAccountNumber()).get(0)
				.getBalance();		
		assertTrue(acc1BalanceBeforeTransfer.compareTo(acc1BalanceAfterTransfer) == 0);
		assertTrue(acc2BalanceBeforeTransfer.compareTo(acc2BalanceAfterTransfer) == 0);
	}

	/*
	 * When creating a transfer transaction from account1 to account2, and account2
	 * does not exist, then operation should fail
	 */
	@WithMockUser(MOCK_USER)
	@Test
	public void givenAcc2NotExist_whenTransferMoneyFromAcc1ToAcc2_thenFailed() throws Exception {
		String account2Number = "222222";
		assertNotExisted(account2Number);
		BigDecimal acc1BalanceBeforeTransfer = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();

		mockMvc.perform(get(TRANSFER_PATH + "/validateAccount").param("destAccountNum", account2Number))
				.andExpect(model().attribute(ERROR_HOLDER, "Invalid account"));

		BigDecimal acc1BalanceAfterTransfer = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();		
		assertTrue(acc1BalanceBeforeTransfer.compareTo(acc1BalanceAfterTransfer) == 0);
	}
}
