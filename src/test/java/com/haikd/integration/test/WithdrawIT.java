package com.haikd.integration.test;

import static com.haikd.constant.Constant.*;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.haikd.domain.Account;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration")
public class WithdrawIT extends CommonIT {

	/*
	 * When creating a withdraw transaction to an account, this account is expected
	 * to have its balance reduced by the amount indicated in the withdraw
	 */
	@WithMockUser(MOCK_USER)
	@Test
	public void givenWithdrawTransaction_whenWithdraw_thenBalaceReducedByAmount() throws Exception {
		createAccountAndSaveToDb();
		String amount = "20";
		BigDecimal balanceBeforeWithdraw = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();

		mockMvc.perform(get(WITHDRAW_PATH + "/doWithdraw").param("amount", amount));

		BigDecimal balanceAfterWithdraw = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();
		assertTrue(balanceBeforeWithdraw.subtract(new BigDecimal(amount)).compareTo(balanceAfterWithdraw) == 0);
	}

	private void createAccountAndSaveToDb() {
		Account account = new Account();
		account.setAccountNumber(MOCK_USER);
		account.setPin(MOCK_USER);
		account.setName("mock");
		account.setBalance(new BigDecimal(600));
		accRepository.save(account);
	}

	/*
	 * When creating a withdraw transaction to an account, and the account does not
	 * have enough funds, it's expected to receive an error
	 */
	@WithMockUser(MOCK_USER)
	@Test
	public void givenNotEnoughFundAccount_whenWithdraw_thenFailed() throws Exception {
		createAccountAndSaveToDb();
		String amount = "700";
		BigDecimal balanceBeforeWithdraw = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();

		mockMvc.perform(get(WITHDRAW_PATH + "/doWithdraw").param("amount", amount))
				.andExpect(model().attribute(ERROR_HOLDER, "Insufficient balance $" + balanceBeforeWithdraw));

		BigDecimal balanceAfterWithdraw = accRepository.findByAccountNumber(MOCK_USER).get(0).getBalance();
		assertTrue(balanceBeforeWithdraw.compareTo(balanceAfterWithdraw) == 0);
	}
}
