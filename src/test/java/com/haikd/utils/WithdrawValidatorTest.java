package com.haikd.utils;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import com.haikd.domain.Account;
import com.haikd.repositories.AccountRepository;

@RunWith(SpringRunner.class)
public class WithdrawValidatorTest {
	@Mock
	AccountRepository accountRepo;
	@InjectMocks
	WithdrawValidator validator;
	List<Account> accs;
	String accNum;

	@Before
	public void setUp() {
		accNum = "111111";
		accs = this.createMockValues(accNum);
	}

	@Test
	public void givenInvalidAmount_whenValidate_thenShowErrorMessage() {
		String amount = "a";
		this.validator.validate(amount, accNum);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Invalid amount"));
	}

	/**
	 * When creating a withdraw transaction to an account, and the account does not
	 * have enough funds, it's expected to receive an error
	 * 
	 */
	@Test
	public void givenAmountIsGreaterThanAccountBalance_whenValidate_thenShowErrorMessage() {
		String amount = "201";
		Mockito.when(this.accountRepo.findByAccountNumber(accNum)).thenReturn(accs);
		this.validator.validate(amount, accNum);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Insufficient balance $" + accs.get(0).getBalance()));
	}

	@Test
	public void givenAmountLessThanAccountBalance_whenValidate_thenGiveNoError() {
		String amount = "190";
		this.validator.validate(amount, accNum);
		assertTrue(!this.validator.hasError());
		assertTrue(this.validator.getError() == null);
	}

	private List<Account> createMockValues(String accNum) {
		List<Account> accs = new ArrayList<Account>();
		Account mockAcc = new Account();
		mockAcc.setAccountNumber(accNum);
		mockAcc.setBalance(new BigDecimal(200));
		accs.add(mockAcc);
		Mockito.when(this.accountRepo.findByAccountNumber(accNum)).thenReturn(accs);
		return accs;
	}
}
