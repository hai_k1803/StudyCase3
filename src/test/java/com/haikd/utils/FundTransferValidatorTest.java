package com.haikd.utils;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import com.haikd.domain.Account;
import com.haikd.repositories.AccountRepository;

@RunWith(SpringRunner.class)
public class FundTransferValidatorTest {
	@Mock
	AccountRepository accountRepo;
	@InjectMocks
	FundTransferValidator validator;
	List<Account> transferingAccs;
	String transferingAccNum;

	@Before
	public void setUp() {
		transferingAccNum = "111111";
		transferingAccs = this.createMockValues(transferingAccNum);
	}

	@Test
	public void givenInvalidAccountNumber_whenValidate_thenShowErrorMessage() {
		String destinationAccNum = "12345a";
		this.validator.validateAccount(destinationAccNum);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Invalid account"));
	}

	/**
	 * When creating a transfer transaction from account1 to account2, and account2
	 * does not exist, then operation should fail
	 */
	@Test
	public void givenNotExistedAccount_whenValidate_thenShowErrorMessage() {
		String destinationAccNum = "123456";
		List<Account> empty = new ArrayList<Account>();
		Mockito.when(this.accountRepo.findByAccountNumber(destinationAccNum)).thenReturn(empty);
		this.validator.validateAccount(destinationAccNum);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Invalid account"));
	}

	@Test
	public void givenInvalidAmount_whenValidate_thenShowErrorMessage() {
		String amount = "a";
		this.validator.validateAmount(amount);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Invalid amount"));
	}

	/**
	 * When creating a transfer transaction from account1 to account2, and account1
	 * does not have enough funds, operation should throw an error
	 */
	@Test
	public void givenAmountIsGreaterThanAccountBalance_whenValidate_thenShowErrorMessage() {
		String amount = "201";
		Mockito.when(this.accountRepo.findByAccountNumber(transferingAccNum)).thenReturn(transferingAccs);
		this.validator.validate(amount, transferingAccNum);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Insufficient balance $" + transferingAccs.get(0).getBalance()));
	}

	private List<Account> createMockValues(String accNum) {
		List<Account> accs = new ArrayList<Account>();
		Account mockAcc = new Account();
		mockAcc.setAccountNumber(accNum);
		mockAcc.setBalance(new BigDecimal(200));
		accs.add(mockAcc);
		Mockito.when(this.accountRepo.findByAccountNumber(accNum)).thenReturn(accs);
		return accs;
	}

	@Test
	public void givenAmountIsGreaterThanMax_whenValidate_thenShowErrorMessage() {
		String amount = "1010";
		this.validator.validateAmount(amount, transferingAccNum);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Maximum amount to withdraw is $" + Constant.MAX_AMOUNT));
	}

	@Test
	public void givenAmountIsLessThanMax_whenValidate_thenShowErrorMessage() {
		String amount = "0";
		this.validator.validateAmount(amount, transferingAccNum);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Minimum amount to withdraw is $" + Constant.MIN_AMOUNT));
	}

}
