package com.haikd.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class OtherWithdrawValidatorTest {
	OtherWithdrawValidator validator;

	@Before
	public void setUp() {
		validator = new OtherWithdrawValidator();
	}

	@Test
	public void givenInvalidAmountIsNotMultipleOf10_whenValidate_thenShowErrorMessage() {
		String amount = "11";
		this.validator.validateAmount(amount);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Invalid amount"));
	}

	@Test
	public void givenInvalidAmountIsGreaterThanMax_whenValidate_thenShowErrorMessage() {
		String amount = "1010";
		this.validator.validateAmount(amount);
		assertTrue(this.validator.hasError());
		assertTrue(this.validator.getError().equals("Maximum amount to withdraw is $" + Constant.MAX_AMOUNT));
	}

	@Test
	public void givenvalidAmount_whenValidate_thenReturnNoError() {
		String amount = "900";
		this.validator.validateAmount(amount);
		assertTrue(!this.validator.hasError());
		assertTrue(this.validator.getError() == null);
	}

}
