package com.haikd.controllers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static com.haikd.constant.Constant.*;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.haikd.constant.Constant;
import com.haikd.domain.Account;
import com.haikd.domain.Transaction;
import com.haikd.repositories.AccountRepository;
import com.haikd.service.WithdrawService;
import com.haikd.utils.WithdrawValidator;

@RunWith(SpringRunner.class)
@WebMvcTest(WithdrawController.class)
@ActiveProfiles("test")
public class WithdrawControllerTest {
	@Autowired
	MockMvc mockMvc;
	@MockBean
	WithdrawValidator validator;
	@MockBean
	WithdrawService service;
	@MockBean
	AccountRepository accRepo;

	@Test
	public void givenUnauthenticatedUser_whenOpenWithdrawPage_thenRedirectToLoginPage() throws Exception {
		mockMvc.perform(get(WITHDRAW_PATH)).andExpect(status().is(UNAUTHORIZED));
	}

	@WithMockUser(MOCK_USER)
	@Test
	public void givenAuthenticatedUser_whenOpenWithdrawPage_thenOk() throws Exception {
		mockMvc.perform(get(WITHDRAW_PATH)).andExpect(status().isOk()).andExpect(view().name(WITHDRAW_PAGE));
	}

	@WithMockUser(MOCK_USER)
	@Test
	public void givenInvalidAmount_doWithdraw_thenFailed() throws Exception {
		String mockAccountNum = Constant.MOCK_USER;
		String invalidAmount = "aa";
		String mockError = "mockError";
		doNothing().when(this.validator).validate(invalidAmount, mockAccountNum);
		when(this.validator.hasError()).thenReturn(true);
		when(this.validator.getError()).thenReturn(mockError);

		mockMvc.perform(get(WITHDRAW_PATH + "/doWithdraw").param("amount", invalidAmount)).andExpect(status().isOk())
				.andExpect(view().name(WITHDRAW_PAGE)).andExpect(model().attribute(Constant.ERROR_HOLDER, mockError));
	}

	@WithMockUser(MOCK_USER)
	@Test
	public void givenValidAmount_doWithdraw_thenOk() throws Exception {
		String validAmount = "100";
		Transaction mockTrans = new Transaction();
		Account mockAcc = new Account();
		mockAcc.setBalance(BigDecimal.TEN);
		mockInitialValues(validAmount, mockTrans, mockAcc);

		mockMvc.perform(get(WITHDRAW_PATH + "/doWithdraw").param("amount", validAmount)).andExpect(status().isFound())
				.andExpect(redirectedUrl("/withdraw/summaryWithdraw"))
				.andExpect(flash().attribute("transaction", mockTrans))
				.andExpect(flash().attribute("balance", mockAcc.getBalance()));
	}

	private String mockInitialValues(String validAmount, Transaction mockTrans, Account acc) throws Exception {
		String mockAccountNum = MOCK_USER;
		doNothing().when(this.validator).validate(validAmount, mockAccountNum);
		when(this.validator.hasError()).thenReturn(false);
		when(this.service.withdraw(validAmount, mockAccountNum)).thenReturn(mockTrans);
		when(this.service.getAccountBy(mockAccountNum)).thenReturn(acc);

		return validAmount;
	}
}
