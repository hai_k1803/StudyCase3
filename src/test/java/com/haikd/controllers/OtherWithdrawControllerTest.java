package com.haikd.controllers;

import static com.haikd.constant.Constant.OTHER_WITHDRAW_PAGE;
import static com.haikd.constant.Constant.UNAUTHORIZED;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.haikd.constant.Constant;
import com.haikd.domain.Account;
import com.haikd.domain.Transaction;
import com.haikd.repositories.AccountRepository;
import com.haikd.service.WithdrawService;
import com.haikd.utils.OtherWithdrawValidator;

@RunWith(SpringRunner.class)
@WebMvcTest(OtherWithdrawController.class)
@ActiveProfiles("test")
public class OtherWithdrawControllerTest {

	private final static String OTHER_PATH = "/other";
	@Autowired
	MockMvc mockMvc;
	@MockBean
	OtherWithdrawValidator validator;
	@MockBean
	WithdrawService service;
	@MockBean
	AccountRepository accRepo;

	@Test
	public void givenUnauthenticatedUser_whenOpenOtherWithdrawPage_thenRedirectToLoginPage() throws Exception {
		mockMvc.perform(get(OTHER_PATH)).andExpect(status().is(UNAUTHORIZED));
	}

	@WithMockUser("USER")
	@Test
	public void givenAuthenticatedUser_whenOpenOtherWithdrawPage_thenOk() throws Exception {
		mockMvc.perform(get(OTHER_PATH)).andExpect(status().isOk()).andExpect(view().name(OTHER_WITHDRAW_PAGE));
	}

	@WithMockUser("USER")
	@Test
	public void givenAuthenticatedUserAndInvalidAmount_doOtherWithdraw_thenFailed() throws Exception {
		String mockAccountNum = "USER";
		String invalidAmount = "aaaaaa";
		String mockError = "mockError";
		doNothing().when(this.validator).validate(invalidAmount, mockAccountNum);
		when(this.validator.hasError()).thenReturn(true);
		when(this.validator.getError()).thenReturn(mockError);

		mockMvc.perform(get(OTHER_PATH + "/doWithdraw").param("amount", invalidAmount)).andExpect(status().isOk())
				.andExpect(view().name(OTHER_WITHDRAW_PAGE))
				.andExpect(model().attribute(Constant.ERROR_HOLDER, mockError));
	}

	@WithMockUser("USER")
	@Test
	public void givenAuthenticatedUserAndValidAmount_doOtherWithdraw_thenOk() throws Exception {
		String validAmount = "100";
		Transaction mockTrans = new Transaction();
		Account mockAcc = new Account();
		mockAcc.setBalance(BigDecimal.TEN);
		mockInitialValues(validAmount, mockTrans, mockAcc);

		mockMvc.perform(get(OTHER_PATH + "/doWithdraw").param("amount", validAmount))
				.andExpect(view().name("redirect:/withdraw/summaryWithdraw"))
				.andExpect(MockMvcResultMatchers.flash().attribute("transaction", mockTrans))
				.andExpect(MockMvcResultMatchers.flash().attribute("balance", mockAcc.getBalance()));
	}

	private String mockInitialValues(String validAmount, Transaction mockTrans, Account acc) throws Exception {
		String mockAccountNum = "USER";
		doNothing().when(this.validator).validate(validAmount, mockAccountNum);
		when(this.validator.hasError()).thenReturn(false);
		when(this.service.withdraw(validAmount, mockAccountNum)).thenReturn(mockTrans);
		when(this.service.getAccountBy(mockAccountNum)).thenReturn(acc);

		return validAmount;
	}
}
