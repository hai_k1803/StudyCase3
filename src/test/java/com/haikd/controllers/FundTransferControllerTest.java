package com.haikd.controllers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static com.haikd.constant.Constant.*;

import com.haikd.repositories.AccountRepository;
import com.haikd.service.FundTransferService;
import com.haikd.utils.FundTransferValidator;

@RunWith(SpringRunner.class)
@WebMvcTest(FundTransferController.class)
@ActiveProfiles("test")
public class FundTransferControllerTest {
	private static final String TRANSFER_FOLDER_PATH = "fundTransfer/";
	@Autowired
	MockMvc mockMvc;
	@MockBean
	FundTransferValidator validator;
	@MockBean
	FundTransferService service;
	@MockBean
	AccountRepository accRepo;

	@Test
	public void givenUnauthenticatedUser_whenOpenTransferPage_thenUnableToAccess() throws Exception {
		mockMvc.perform(get(TRANSFER_PATH)).andExpect(status().is(UNAUTHORIZED));
	}

	@WithMockUser("USER")
	@Test
	public void givenAuthenticatedUser_whenOpenTransferPage_thenOk() throws Exception {
		mockMvc.perform(get(TRANSFER_PATH)).andExpect(status().isOk())
				.andExpect(view().name(TRANSFER_FOLDER_PATH + "destAccountInput"));
	}

	@Test
	public void givenUnauthenticatedUser_whenCheckValidAccount_thenUnableToAccess() throws Exception {
		mockMvc.perform(get(TRANSFER_PATH + "/validateAccount")).andExpect(status().is(UNAUTHORIZED));
	}

	@WithMockUser("USER")
	@Test
	public void givenInvalidAccount_whenCheckValidAccount_thenReturnError() throws Exception {
		String invalidAcc = "invalidAcc";
		String mockAccountNum = "USER";
		String invalidAmount = "aaaaaa";
		String mockError = "mockError";
		doNothing().when(this.validator).validate(invalidAmount, mockAccountNum);
		when(this.validator.hasError()).thenReturn(true);
		when(this.validator.getError()).thenReturn(mockError);
		mockMvc.perform(get(TRANSFER_PATH + "/validateAccount").param("destAccountNum", invalidAcc))
				.andExpect(status().isOk()).andExpect(view().name(TRANSFER_FOLDER_PATH + "destAccountInput"))
				.andExpect(model().attribute(ERROR_HOLDER, mockError));
	}

	@Test
	public void givenUnauthenticatedUser_whenGenerateReference_thenUnableToAccess() throws Exception {
		mockMvc.perform(get(TRANSFER_PATH + "/generateReference")).andExpect(status().is(UNAUTHORIZED));
	}

	@Test
	public void givenUnauthenticatedUser_whenShowConfirm_thenUnableToAccess() throws Exception {
		mockMvc.perform(get(TRANSFER_PATH + "/showConfirm")).andExpect(status().is(UNAUTHORIZED));
	}

	@Test
	public void givenUnauthenticatedUser_whenConfirm_thenUnableToAccess() throws Exception {
		mockMvc.perform(get(TRANSFER_PATH + "/confirm")).andExpect(status().is(UNAUTHORIZED));
	}

	@Test
	public void givenUnauthenticatedUser_whenCancel_thenUnableToAccess() throws Exception {
		mockMvc.perform(get(TRANSFER_PATH + "/cancel")).andExpect(status().is(UNAUTHORIZED));
	}

	@Test
	public void givenUnauthenticatedUser_whenCheckValidAmount_thenUnableToAccess() throws Exception {
		mockMvc.perform(get(TRANSFER_PATH + "/checkValidAmount")).andExpect(status().is(UNAUTHORIZED));
	}

}
