package com.haikd.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static com.haikd.constant.Constant.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.haikd.domain.Transaction;
import com.haikd.repositories.AccountRepository;
import com.haikd.service.TransactionService;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
@ActiveProfiles("test")
public class TransactionControllerTest {

	private static final String TRANSACTION_PATH = "/transaction";
	@Autowired
	MockMvc mockMvc;
	@MockBean
	TransactionService service;
	@MockBean
	AccountRepository accRepo;

	@Test
	public void givenUnauthenticatedUser_whenOpenTransactionPage_thenRedirectToLoginPage() throws Exception {
		mockMvc.perform(get(TRANSACTION_PATH)).andExpect(status().is(UNAUTHORIZED));
	}

	@WithMockUser("USER")
	@Test
	public void givenAuthenticatedUser_whenFetchLast10Transactions_thenReturnLast10Transactions() throws Exception {
		List<Transaction> mockTransactions = this.initTransactions();
		Mockito.when(service.getLastTransactions(Mockito.anyString())).thenReturn(mockTransactions);

		mockMvc.perform(get(TRANSACTION_PATH + "/getLastTransactions")).andExpect(status().isOk())
				.andExpect(view().name(LAST_TRANSACTIONS_PAGE)).andExpect(model().size(1))
				.andExpect(model().attribute("transactions", mockTransactions));
	}

	private List<Transaction> initTransactions() {
		List<Transaction> transactions = new ArrayList<Transaction>();
		Transaction trans1 = new Transaction();
		Transaction trans2 = new Transaction();
		Transaction trans3 = new Transaction();
		transactions.add(trans1);
		transactions.add(trans2);
		transactions.add(trans3);

		return transactions;
	}

//	@WithMockUser("USER")
//	@Test
//	public void givenPage1_whenFetchAllTransactions_thenReturn10Transactions() throws Exception {
//		setupMockMVC();
//		List<Transaction> mockTransactions = this.createTransactions();
//		Page<Transaction> tranPage = new PageImpl<>(mockTransactions, new PageRequest(0, 2), 1);
//		Mockito.when(service.findPaginated(Mockito.any(PageRequest.class), "USER").thenReturn(tranPage);
//
//		mockMvc.perform(get(TRANSACTION_PATH + "/getAllTransactions/page/1")).andExpect(status().isOk())
//				.andExpect(view().name(Constant.ALL_TRANSACTIONS_PAGE)).andExpect(model().size(1))
//				.andExpect(model().attribute("transactions", mockTransactions));
//	}

}
