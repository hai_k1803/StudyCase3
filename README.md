# StudyCase3
Bootify The Application
## Technologies
- Spring Boot - 2.x.x
- Java - 1.8.x
- Maven - 4.x.x
- SQL Server - 5.x.x
- JPA
- JSP
- H2 database for integration test.
## Installation
1. Clone application from git
2. Run sql script: CREATE DATABASE myTest
3. mvn spring-boot:run
5. Login with the account(Number:111111, Password:111111)  
6. The app run at http://localhost:8080/myTest.
## Automated Tests
- The maven-failsafe-plugin has been added to the [pom.xml] file to automate the integration tests. It executes the integration test during the integration-test phase (in contrast to the maven-surefire-plugin that executes tests in the test phase). Moreover, it is activated by using the Maven itest profile.
- the integration tests will use and embedded H2 database
- Run mvn test to run unit tests
- Run mvn integration-test to run all tests (unit tests and integration tests)
